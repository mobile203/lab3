import 'dart:io';
import 'package:ex_1/ex_1.dart';
import 'calculator.dart';

void main(List<String> args) {
  int? menu;
  Calculator cal = new Calculator();
  print('MENU');
  print('Select the choice you want to perform : ');
  print('1.ADD');
  print('2.SUBTRACT');
  print('3.MULTIPLY');
  print('4.DIVIDE');
  print('5.EXIT');

  print("Choice you want to enter : ");
  menu = int.parse(stdin.readLineSync()!);

  switch (menu) {
    case 1:
      {
        print("Enter the value for x : ");
        double x = double.parse(stdin.readLineSync()!);

        print("Enter the value for y : ");
        double y = double.parse(stdin.readLineSync()!);
        print("Sum of the two numbers is : ");
        print(cal.add(x, y));
      }
      break;
    case 2:
      {
        print("Enter the value for x : ");
        double x = double.parse(stdin.readLineSync()!);

        print("Enter the value for y : ");
        double y = double.parse(stdin.readLineSync()!);
        print("Sum of the two numbers is : ");
        print(cal.minus(x, y));
      }
      break;
    case 3:
      {
        print("Enter the value for x : ");
        double x = double.parse(stdin.readLineSync()!);

        print("Enter the value for y : ");
        double y = double.parse(stdin.readLineSync()!);
        print("Sum of the two numbers is : ");
        print(cal.multiply(x, y));
      }
      break;
    case 4:
      {
        print("Enter the value for x : ");
        double x = double.parse(stdin.readLineSync()!);

        print("Enter the value for y : ");
        double y = double.parse(stdin.readLineSync()!);
        print("Sum of the two numbers is : ");
        print(cal.divide(x, y));
      }
      break;
    case 5:
      {
        print("Thank You");
      }
      break;
  }
}
  // print("Choice you want to enter : ");
  // menu = int.parse(stdin.readLineSync()!);

  // print('Enter the value of x : ');
  // int? x = int.parse(stdin.readLineSync()!);
  // cal.add();
  // print('Enter the value of y : ');
  // int? y = int.parse(stdin.readLineSync()!);

