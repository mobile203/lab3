import 'dart:async';
import 'dart:ffi';
import 'dart:io';

void main(List<String> args) {
  int? menu;
  print("TaoBin Beverage");
  print('MENU');
  String? type;
  String? sweetness;
  String? pay;
  String? member;
  String? phone;

  print("Enter your Menu : ");
  menu = int.parse(stdin.readLineSync()!);
  switch (menu) {
    case 1:
      {
        print("Coffee");
        print("Please select your menu :");
        type = stdin.readLineSync();

        var arr = ['Americano', 'Espresso', 'Cappuccino', 'Mocha', 'Latte'];
        if (type == 'Americano' ||
            type == 'Espresso' ||
            type == 'Cappucino' ||
            type == 'Mocha' ||
            type == 'Latte') {
          print("You can choose sweetness level :");
          print('0%');
          print('25%');
          print('50%');
          print('75%');
          print('100%');
          print("Please select sweetness level :");
          sweetness = stdin.readLineSync();
        } else {
          print("Sorry, this product is out of stock.");
          break;
        }
        print('Price: 50');
        print('Payment Method : ');
        pay = stdin.readLineSync();
        var arr2 = [
          'Cash',
          'QRCode',
          'ShopeePay',
          'Use TaoBin credit',
          'Use/View coupons'
        ];
        if (pay == 'Cash') {
          print('Please put money in the banknote compartment');
          print('');
        } else if (pay == 'QRCode') {
          print('scan to pay');
          print('');
        } else if (pay == 'ShopeePay') {
          print('Click on shopeePay and scan to pay');
          print('');
        } else if (pay == 'Use TaoBin credit') {
          print('Use TaoBin credit');
          print('');
        } else if (pay == 'Use/View coupons') {
          print('Use and View');
          print('');
        } else {
          print('Sorry, Error');
          print('Thank You');
        }
        print('Are you a TaoBin member?');
        if (member == 'Yes') {
          member = stdin.readLineSync();
          print('Input your TaoBin member?');
        } else {}
        member = stdin.readLineSync();
        print('Input your TaoBin member?');
        phone = stdin.readLineSync();
      }

      break;

    case 2:
      {
        print("Tea");
        print("Please select your menu :");
        type = stdin.readLineSync();

        var arr = [
          'Thai Milk Tea',
          'Taiwanese Milk Tea',
          'Matcha Latte',
          'Iced Tea',
          'Limeade Tea'
        ];
        if (type == 'Thai Milk Tea' ||
            type == 'Taiwanese Milk Tea' ||
            type == 'Matcha Latte' ||
            type == 'Iced Tea' ||
            type == 'Limeade Tea') {
          print("You can choose sweetness level :");
          print('0%');
          print('25%');
          print('50%');
          print('75%');
          print('100%');
          print("Please select sweetness level :");
          sweetness = stdin.readLineSync();
        } else {
          print("Sorry, this product is out of stock.");
          break;
        }
        print('Price: 60');
        print('Payment Method : ');
        pay = stdin.readLineSync();
        var arr2 = [
          'Cash',
          'QRCode',
          'ShopeePay',
          'Use TaoBin credit',
          'Use/View coupons'
        ];
        if (pay == 'Cash') {
          print('Please put money in the banknote compartment');
          print('');
        } else if (pay == 'QRCode') {
          print('scan to pay');
          print('');
        } else if (pay == 'ShopeePay') {
          print('Click on shopeePay and scan to pay');
          print('');
        } else if (pay == 'Use TaoBin credit') {
          print('Use TaoBin credit');
          print('');
        } else if (pay == 'Use/View coupons') {
          print('Use and View');
          print('');
        } else {
          print('Sorry, Error');
          print('Thank You');
        }
        print('Are you a TaoBin member?');
        if (member == 'Yes') {
          member = stdin.readLineSync();
          print('Input your TaoBin member?');
        } else {}
        member = stdin.readLineSync();
        print('Input your TaoBin member?');
        phone = stdin.readLineSync();
      }

      break;
    case 3:
      {
        print("Milk Cocoa and Caramel");
        print("Please select your menu :");
        type = stdin.readLineSync();

        var arr = [
          'Cocoa',
          'Milk',
          'Pink Milk',
          'Iced Caramel Milk',
          'Kokuto Milk'
        ];
        if (type == 'Cocoa' ||
            type == 'Milk' ||
            type == 'Pink Milk' ||
            type == 'Iced Caramel Milk' ||
            type == 'Kokuto Milk') {
          print("You can choose sweetness level :");
          print('0%');
          print('25%');
          print('50%');
          print('75%');
          print('100%');
          print("Please select sweetness level :");
          sweetness = stdin.readLineSync();
        } else {
          print("Sorry, this product is out of stock.");
          break;
        }
        print('Price: 55');
        print('Payment Method : ');
        pay = stdin.readLineSync();
        var arr2 = [
          'Cash',
          'QRCode',
          'ShopeePay',
          'Use TaoBin credit',
          'Use/View coupons'
        ];
        if (pay == 'Cash') {
          print('Please put money in the banknote compartment');
          print('');
        } else if (pay == 'QRCode') {
          print('scan to pay');
          print('');
        } else if (pay == 'ShopeePay') {
          print('Click on shopeePay and scan to pay');
          print('');
        } else if (pay == 'Use TaoBin credit') {
          print('Use TaoBin credit');
          print('');
        } else if (pay == 'Use/View coupons') {
          print('Use and View');
          print('');
        } else {
          print('Sorry, Error');
          print('Thank You');
        }
        print('Are you a TaoBin member?');
        if (member == 'Yes') {
          member = stdin.readLineSync();
          print('Input your TaoBin member?');
        } else {}
        member = stdin.readLineSync();
        print('Input your TaoBin member?');
        phone = stdin.readLineSync();
      }

      break;
    case 4:
      {
        print("Protein Check");
        print("Please select your menu :");
        type = stdin.readLineSync();

        var arr = [
          'Matcha Protein Shake',
          'Chocolate Protein Shake',
          'Strawberry Protein Shake',
          'Espresso Protein Shake',
          'Brown Sugar Protein Shake'
              'Plain Protein Shake'
        ];
        if (type == 'Matcha Protein Shake' ||
            type == 'Chocolate Protein Shake' ||
            type == 'Strawberry Protein Shake' ||
            type == 'Espresso Protein Shake' ||
            type == 'Brown Sugar Protein Shake' ||
            type == 'Plain Protein Shake') {
          print("You can choose sweetness level :");
          print('0%');
          print('25%');
          print('50%');
          print('75%');
          print('100%');
          print("Please select sweetness level :");
          sweetness = stdin.readLineSync();
        } else {
          print("Sorry, this product is out of stock.");
          break;
        }
        print('Price: 70');
        print('Payment Method : ');
        pay = stdin.readLineSync();
        var arr2 = [
          'Cash',
          'QRCode',
          'ShopeePay',
          'Use TaoBin credit',
          'Use/View coupons'
        ];
        if (pay == 'Cash') {
          print('Please put money in the banknote compartment');
          print('');
        } else if (pay == 'QRCode') {
          print('scan to pay');
          print('');
        } else if (pay == 'ShopeePay') {
          print('Click on shopeePay and scan to pay');
          print('');
        } else if (pay == 'Use TaoBin credit') {
          print('Use TaoBin credit');
          print('');
        } else if (pay == 'Use/View coupons') {
          print('Use and View');
          print('');
        } else {
          print('Sorry, Error');
          print('Thank You');
        }
        print('Are you a TaoBin member?');
        if (member == 'Yes') {
          member = stdin.readLineSync();
          print('Input your TaoBin member?');
        } else {}
        member = stdin.readLineSync();
        print('Input your TaoBin member?');
        phone = stdin.readLineSync();
      }

      break;
    case 5:
      {
        print("Soda and etc.");
        print("Please select your menu :");
        type = stdin.readLineSync();

        var arr = [
          'Strawberry',
          'Blueberry',
          'Pepsi',
          'Limenade Soda',
          'Lychee Soda'
              'Iced Sala'
        ];
        if (type == 'Strawberry' ||
            type == 'Blueberry' ||
            type == 'Strawberry Protein Shake' ||
            type == 'Pepsi' ||
            type == 'Limenade Soda' ||
            type == 'Iced Sala') {
          print("You can choose sweetness level :");
          print('0%');
          print('25%');
          print('50%');
          print('75%');
          print('100%');
          print("Please select sweetness level :");
          sweetness = stdin.readLineSync();
        } else {
          print("Sorry, this product is out of stock.");
          break;
        }
        print('Price: 50');
        print('Payment Method : ');
        pay = stdin.readLineSync();
        var arr2 = [
          'Cash',
          'QRCode',
          'ShopeePay',
          'Use TaoBin credit',
          'Use/View coupons'
        ];
        if (pay == 'Cash') {
          print('Please put money in the banknote compartment');
          print('');
        } else if (pay == 'QRCode') {
          print('scan to pay');
          print('');
        } else if (pay == 'ShopeePay') {
          print('Click on shopeePay and scan to pay');
          print('');
        } else if (pay == 'Use TaoBin credit') {
          print('Use TaoBin credit');
          print('');
        } else if (pay == 'Use/View coupons') {
          print('Use and View');
          print('');
        } else {
          print('Sorry, Error');
          print('Thank You');
        }
        print('Are you a TaoBin member?');
        if (member == 'Yes') {
          member = stdin.readLineSync();
          print('Input your TaoBin member?');
        } else {}
        member = stdin.readLineSync();
        print('Input your TaoBin member?');
        phone = stdin.readLineSync();
      }

      break;
  }

  print('Your Order is ' + type!);
  print('The sweetness level you choose is ' + sweetness!);
  print('Price: 50');
  print('Payment Method ' + pay!);
  print('Your member : ' + phone!);
}
