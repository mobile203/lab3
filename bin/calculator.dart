import 'dart:io';
import 'dart:math';

class Calculator {
  double x = 0;
  double y = 0;
  double sum = 0;

  calculator(double x, double y) {
    this.x = x;
    this.y = y;
    this.sum = sum;
  }

  double add(double x, double y) {
    return x + y;
  }

  double minus(double x, double y) {
    return x - y;
  }

  double multiply(double x, double y) {
    return x * y;
  }

  double divide(double x, double y) {
    if (y == 0) {
      print("Error!");
      return 0;
    } else {
      return x / y;
    }
  }
}
